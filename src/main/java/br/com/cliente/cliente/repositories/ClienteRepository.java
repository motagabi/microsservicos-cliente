package br.com.cliente.cliente.repositories;

import br.com.cliente.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
    Optional<Cliente> findById(int id);


}
